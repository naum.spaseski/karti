/**
 * Created by nomce on 22/06/15.
 */

 //on start
 var jsonExport // = new Object();
 var nbPlayers = 2;
 var nbGameGlobal = 0;
 var timLevo,pismoLevo,timDesno,pismoDesno;

$(document).ready(function($) {


  var user=getCookie("tablaData");
  if ( user != '' ){
    var jsonImport;
        jsonImport = JSON.parse( user );
        var i, j;
        for (i = 1; i <= nbPlayers; i++) {
            $('#input-0-' + i).val(jsonImport.iminja[i]);
        }
        $('#dugmeStart').click();

        for (i = 1; i <= jsonImport.levoPismo; i++){
          $('#btn-pismo-levo').click();
        }

        for (i = 1; i <= jsonImport.desnoPismo; i++){
          $('#btn-pismo-desno').click();
        }

        for (i = 1; i <= jsonImport.nbGameGlobal; i++) {
          for (j = 1; j <= nbPlayers; j++) {
              $('#input-' + i + '-' + j).val(jsonImport.igra[i][j]);
          }
          $('#btn-win').click();
        }

  }
});

$('#dugmeStart').click(function() {

  jsonExport = new Object();
  jsonExport.iminja = [];
  jsonExport.igra = [];
  pismoLevo = 0;
  pismoDesno = 0;
  timLevo = 0;
  timDesno = 0;

  $('#dugminja').removeClass("hidden");
  $('#pisma').removeClass("hidden");
  $('#X-1').removeClass("hidden");
  var ime;
  for (i = 1; i <= nbPlayers; i++) {
      $('#1-' + i).removeClass("hidden");
      ime = $('#input-0-' + i).val();
      jsonExport.iminja[i] = ime;
      $('#0-' + i).html( ime );
  }
  nbGameGlobal++;
  $( 'tbody' ).append( generateRow(nbGameGlobal) );
  $('#dugmeStart').addClass('hidden');
  $('#naslov').append(' (<span id="brojPartii">' + nbGameGlobal + '</span>)');
});

$('#btn-pismo-levo').click(function() {
  pismoLevo = pismoLevo + 1;
  $('#pismo-levo div span').html(pismoLevo);
  jsonExport.levoPismo = pismoLevo;
  setCookie("tablaData", JSON.stringify( jsonExport ), 365);
});

$('#btn-pismo-desno').click(function() {
  pismoDesno = pismoDesno + 1;
  $('#pismo-desno div span').html(pismoDesno);
  jsonExport.desnoPismo = pismoDesno;
  setCookie("tablaData", JSON.stringify( jsonExport ), 365);
});

$('#btn-pismo-levo-brisi').click(function() {
  if (pismoLevo > 0){
    pismoLevo = pismoLevo - 1;
  }
  $('#pismo-levo div span').html(pismoLevo);
  jsonExport.levoPismo = pismoLevo;
  setCookie("tablaData", JSON.stringify( jsonExport ), 365);
});

$('#btn-pismo-desno-brisi').click(function() {
  if (pismoDesno > 0){
    pismoDesno = pismoDesno - 1;
  }
  $('#pismo-desno div span').html(pismoDesno);
  jsonExport.desnoPismo = pismoDesno;
  setCookie("tablaData", JSON.stringify( jsonExport ), 365);
});

$('#btn-win').click(function() {
  var sumaLevo, sumaDesno;
  jsonExport.igra[nbGameGlobal] = [];
    sumaLevo = parseInt($('#input-' + nbGameGlobal + '-1' ).val(),10);
    sumaDesno = parseInt($('#input-' + nbGameGlobal + '-2' ).val(),10);
    if (!sumaLevo || sumaLevo > 25){
      sumaLevo = 25 - sumaDesno;
    }
    if (!sumaDesno || sumaDesno > 25){
      sumaDesno = 25 - sumaLevo;
    }
    if (sumaDesno + sumaLevo == 25 || sumaDesno + sumaLevo == 22){

      jsonExport.igra[nbGameGlobal][1] = parseInt(sumaLevo,10);
      jsonExport.igra[nbGameGlobal][2] = parseInt(sumaDesno,10);
      jsonExport.nbGameGlobal = nbGameGlobal;
      $('#' + nbGameGlobal + '-1').html(sumaLevo);
      $('#' + nbGameGlobal + '-2').html(sumaDesno);
      timLevo = timLevo + sumaLevo;
      timDesno = timDesno + sumaDesno;
      nbGameGlobal++;
      if (timLevo + pismoLevo <= 100 && timDesno + pismoDesno <= 100){
        $( 'tbody' ).append( generateRow(nbGameGlobal) );
      }else{
        $( 'tbody' ).append( generateWin() );
      }
      setCookie("tablaData", JSON.stringify( jsonExport ), 365);
      colorateIt(nbGameGlobal-1);
      $('#brojPartii').html(nbGameGlobal);
    }

});

$('#btn-undo').click(function() {
  if (nbGameGlobal>1){
      var local = nbGameGlobal;
      nbGameGlobal--;
      timLevo = timLevo -   $('#' + nbGameGlobal + '-1').html();
      timDesno = timDesno -   $('#' + nbGameGlobal + '-2').html();
      $("#" + nbGameGlobal).remove();
      $("tbody tr:last-child").attr("id",""+nbGameGlobal);
      var player = 1;
      $("tbody tr:last-child > td").each(function () {
        $( this ).attr("id",""+ nbGameGlobal + "-" + player);
        $( this ).children().each(function () {
          $( this ).attr("id","input-"+ nbGameGlobal + "-" + player);
        });
        player++;
      });
      delete jsonExport.igra[nbGameGlobal];
      setCookie("tablaData", JSON.stringify( jsonExport ), 365);
      $('#brojPartii').html(nbGameGlobal);
  }
});

$('#btn-reset').click(function() {
  setCookie("tablaData", '', 365);
  window.location.reload(true);
});

function colorateIt(nbGame){
  var min, max, minI, maxI;
  min = parseInt($('#' + nbGame + '-1').html(),10);
  max = min;
  minI = 1;
  maxI = minI;
  for (i = 2; i <= nbPlayers; i++) {
    if ($('#' + nbGame + '-' + i).html() > max){
      max = parseInt($('#' + nbGame + '-' + i).html());
      maxI = i;
    }else if ($('#' + nbGame + '-' + i).html() < min){
      min = parseInt($('#' + nbGame + '-' + i).html(),10);
      minI = i;
    }
  }
  $('#' + nbGame + '-' + maxI).addClass("green");
}

function generateRow(nbGame){
  var bla;
  bla = '<tr id="' + nbGame + '">\n';
  var i;
  for (i = 1; i <= nbPlayers; i++) {
    bla = bla + '<td id="' + nbGame + '-' + i + '" ><input id="input-'+ nbGame + '-' + i + '" class="form-control" type="number" /></td>\n';
  }
  bla = bla + '</tr>';
  return bla;
}

function generateWin(){
  var totalLevo, totalDesno;
  totalLevo = timLevo + pismoLevo;
  totalDesno = timDesno + pismoDesno;
  var bla,pobedi,razlika;
  pobedi = 1;
  razlika = totalLevo - totalDesno;
  if (totalDesno > totalLevo){
    pobedi = 2;
    razlika = totalDesno - totalLevo;
  }

  bla = '<tr id="total">\n';
  bla = bla + '<td >'+ totalLevo +'</td>\n';
  bla = bla + '<td >'+ totalDesno +'</td>\n';
  bla = bla + '</tr><tr ><td colspan="2" > Тимот '+ jsonExport.iminja[pobedi] +' победи со разлика од '+ razlika +'</td></tr>';
  return bla;
}
